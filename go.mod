module zcsv2vcf

go 1.15

require (
	github.com/emersion/go-vcard v0.0.0-20200508080525-dd3110a24ec2
	github.com/gocarina/gocsv v0.0.0-20200827134620-49f5c3fa2b3e
	github.com/rs/zerolog v1.19.0
)
