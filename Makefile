# Need to set this for some Linux installs to use bash builtins like pushd
SHELL := /bin/bash

# Override this env var when building release
ZCSV2VCF_VERSION?=$(shell git describe --tags --dirty)
ZCSV2VCF_GIT_REV?=$(shell git rev-parse --short HEAD)
ZCSV2VCF_BUILD_DATE?=$(shell date -u +'%FT%T%z')

LDFLAGS=-ldflags "-X main.Version=$(ZCSV2VCF_VERSION) \
				 -X main.GitRevision=$(ZCSV2VCF_GIT_REV) \
				 -X main.BuildDate=$(ZCSV2VCF_BUILD_DATE) -w -s" main.go

bin:
	go build -o bin/zcsv2vcf $(LDFLAGS)

release:
	GOARCH=amd64 GOOS=darwin go build -o build/darwin/zcsv2vcf_$(ZCSV2VCF_VERSION)_darwin_amd64 $(LDFLAGS)
	pushd build/darwin && shasum -a 256 zcsv2vcf_$(ZCSV2VCF_VERSION)_darwin_amd64 > ../sha256sums_$(ZCSV2VCF_VERSION).txt && popd
	pushd build/darwin && mv zcsv2vcf_$(ZCSV2VCF_VERSION)_darwin_amd64 zcsv2vcf && popd
	GOARCH=amd64 GOOS=linux go build -o build/linux/zcsv2vcf_$(ZCSV2VCF_VERSION)_linux_amd64 $(LDFLAGS)
	pushd build/linux && shasum -a 256 zcsv2vcf_$(ZCSV2VCF_VERSION)_linux_amd64 >> ../sha256sums_$(ZCSV2VCF_VERSION).txt && popd
	pushd build/linux && mv zcsv2vcf_$(ZCSV2VCF_VERSION)_linux_amd64 zcsv2vcf
	GOARCH=amd64 GOOS=windows go build -o build/windows/zcsv2vcf_$(ZCSV2VCF_VERSION)_windows_amd64 $(LDFLAGS)
	pushd build/windows && shasum -a 256 zcsv2vcf_$(ZCSV2VCF_VERSION)_windows_amd64 >> ../sha256sums_$(ZCSV2VCF_VERSION).txt && popd
	pushd build/windows && mv zcsv2vcf_$(ZCSV2VCF_VERSION)_windows_amd64 zcsv2vcf.exe && popd

.PHONY: clean
clean:
	rm -rf bin/ build/
