# zcsv2vcf (for Zimbra csv export)

`zcsv2vcf` converts a zimbra-exported Contacts CSV into a VCF file for
import into other contacts applications.

## Installation & Usage

First download an appropriate release for your platform from the
releases page.

Then export your contacts from Zimbra, choosing the `Account Contacts`
format from the drop-down.

Run `zcsv2vcf` with the path to your downloaded Zimbra contacts file as
the first argument, and the path where you would like the vcf file
written as the second path. For example:

```
zcsv2vcf /Users/wryfi/Downloads/Contacts.csv /Users/wryfi/Documents/contacts.vcf
```

You can then import `/Users/wryfi/Documents/contacts.vcf` into a
compatible contact management application.

## Fields

`zcsv2vcf` works with the CSV fields exported by Zimbra's `Account
Contacts` format. The following fields are supported:

```
firstName
lastName
middleName
namePrefix
nameSuffix
fullName
nickname
email
email2
email3
workPhone
homePhone
otherPhone
mobilePhone
mobilePhone2
homeStreet
homeCity
homeState
homePostalCode
homeCountry
workStreet
workCity
workState
workPostalCode
workCountry
otherStreet
otherCity
otherState
otherPostalCode
otherCountry
company
jobTitle
birthday
anniversary
notes
```

## Status

This code lacks unit tests but works for me. Use it at your own risk.
