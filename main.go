package main

import (
	"fmt"
	"github.com/emersion/go-vcard"
	"github.com/gocarina/gocsv"
	"github.com/rs/zerolog/log"
	"os"
	"strings"
)

var (
	GitRevision string
	BuildDate   string
	Version     string
)

type Contact struct {
	FirstName      string `csv:"firstName"`
	LastName       string `csv:"lastName"`
	MiddleName     string `csv:"middleName"`
	NamePrefix     string `csv:"namePrefix"`
	NameSuffix     string `csv:"nameSuffix"`
	DisplayName    string `csv:"fullName"`
	NickName       string `csv:"nickname"`
	PrimaryEmail   string `csv:"email"`
	SecondaryEmail string `csv:"email2"`
	TertiaryEmail  string `csv:"email3"`
	WorkPhone      string `csv:"workPhone"`
	HomePhone      string `csv:"homePhone"`
	OtherPhone     string `csv:"otherPhone"`
	MobilePhone    string `csv:"mobilePhone"`
	MobilePhone2   string `csv:"mobilePhone2"`
	HomeAddress    string `csv:"homeStreet"`
	HomeCity       string `csv:"homeCity"`
	HomeState      string `csv:"homeState"`
	HomeZip        string `csv:"homePostalCode"`
	HomeCountry    string `csv:"homeCountry"`
	WorkAddress    string `csv:"workStreet"`
	WorkCity       string `csv:"workCity"`
	WorkState      string `csv:"workState"`
	WorkZip        string `csv:"workPostalCode"`
	WorkCountry    string `csv:"workCountry"`
	OtherAddress   string `csv:"otherStreet"`
	OtherCity      string `csv:"otherCity"`
	OtherState     string `csv:"otherState"`
	OtherZip       string `csv:"otherPostalCode"`
	OtherCountry   string `csv:"otherCountry"`
	Organization   string `csv:"company"`
	JobTitle       string `csv:"jobTitle"`
	Birthday       string `csv:"birthday"`
	Anniversary    string `csv:"anniversary"`
	Notes          string `csv:"notes"`
}

func main() {
	if len(os.Args) < 3 {
		fmt.Printf("zcsv2vcf version %s %s, %s \n", Version, GitRevision, BuildDate)
		fmt.Printf("syntax: \n \t%s <zimbra_csv> <vcf_output>, e.g. %s zimbra.csv vcards.vcf \n", os.Args[0],
			os.Args[0])
		os.Exit(1)
	}
	csvfile := os.Args[1]
	vcffile := os.Args[2]

	if _, err := os.Stat(csvfile); os.IsNotExist(err) {
		log.Fatal().Err(err).Msg("error opening csv file")
	}

	outfile, err := os.Create(vcffile)
	if err != nil {
		log.Fatal().Err(err).Msg("error creating output vcf file")
	}
	defer outfile.Close()

	enc := vcard.NewEncoder(outfile)

	contacts := csvReader(csvfile)
	for _, contact := range contacts {
		card := vcard.Card{}

		if len(contact.DisplayName) > 0 {
			card.SetValue(vcard.FieldFormattedName, contact.DisplayName)
		} else {
			card.SetValue(vcard.FieldFormattedName, contact.FirstName+" "+contact.LastName)
		}

		if len(contact.NickName) > 0 {
			card.SetValue(vcard.FieldNickname, contact.NickName)
		}

		card.Set(vcard.FieldName, &vcard.Field{
			Value: fmt.Sprintf("%s;%s;%s;%s;%s", contact.LastName, contact.FirstName, contact.MiddleName,
				contact.NamePrefix, contact.NameSuffix),
			Params: map[string][]string{
				vcard.ParamSortAs: {
					contact.LastName + " " + contact.FirstName,
				},
			},
		})

		if len(contact.MobilePhone) > 0 {
			card.Add(vcard.FieldTelephone, &vcard.Field{
				Value: strings.TrimSpace(contact.MobilePhone),
				Params: map[string][]string{
					vcard.ParamType: {
						"cell", "voice", "pref",
					},
				},
			})
		}

		if len(contact.MobilePhone2) > 0 {
			card.Add(vcard.FieldTelephone, &vcard.Field{
				Value: strings.TrimSpace(contact.MobilePhone2),
				Params: map[string][]string{
					vcard.ParamType: {
						"cell", "voice",
					},
				},
			})
		}

		if len(contact.HomePhone) > 0 {
			card.Add(vcard.FieldTelephone, &vcard.Field{
				Value: strings.TrimSpace(contact.HomePhone),
				Params: map[string][]string{
					vcard.ParamType: {"home", "voice"},
				},
			})
		}

		if len(contact.WorkPhone) > 0 {
			card.Add(vcard.FieldTelephone, &vcard.Field{
				Value: strings.TrimSpace(contact.WorkPhone),
				Params: map[string][]string{
					vcard.ParamType: {"work", "voice"},
				},
			})
		}

		if len(contact.OtherPhone) > 0 {
			card.Add(vcard.FieldTelephone, &vcard.Field{
				Value: strings.TrimSpace(contact.OtherPhone),
				Params: map[string][]string{
					vcard.ParamType: {"voice"},
				},
			})
		}

		if len(contact.PrimaryEmail) > 0 {
			card.Add(vcard.FieldEmail, &vcard.Field{
				Value: strings.TrimSpace(contact.PrimaryEmail),
				Params: map[string][]string{
					vcard.ParamType: {"pref"},
				},
			})
		}

		if len(contact.SecondaryEmail) > 0 {
			card.AddValue(vcard.FieldEmail, strings.TrimSpace(contact.SecondaryEmail))
		}

		if len(contact.TertiaryEmail) > 0 {
			card.AddValue(vcard.FieldEmail, strings.TrimSpace(contact.TertiaryEmail))
		}

		if len(contact.HomeAddress) > 0 {
			address := formatAddress(contact.HomeAddress, contact.HomeCity, contact.HomeState, contact.HomeZip,
				contact.HomeCountry)
			card.Add(vcard.FieldAddress, &vcard.Field{
				Value: address,
				Params: map[string][]string{
					vcard.ParamType: {"home"},
				},
			})
		}

		if len(contact.WorkAddress) > 0 {
			address := formatAddress(contact.WorkAddress, contact.WorkCity, contact.WorkState, contact.WorkZip,
				contact.WorkCountry)
			card.Add(vcard.FieldAddress, &vcard.Field{
				Value: address,
				Params: map[string][]string{
					vcard.ParamType: {"work"},
				},
			})
		}

		if len(contact.Organization) > 0 {
			card.AddValue(vcard.FieldOrganization, contact.Organization)
		}

		if len(contact.JobTitle) > 0 {
			card.AddValue(vcard.FieldTitle, contact.JobTitle)
		}

		if len(contact.Birthday) > 0 {
			card.AddValue(vcard.FieldBirthday, contact.Birthday)
		}

		if len(contact.Anniversary) > 0 {
			card.AddValue(vcard.FieldAnniversary, contact.Anniversary)
		}

		if len(contact.Notes) > 0 {
			card.AddValue(vcard.FieldNote, contact.Notes)
		}

		vcard.ToV4(card)
		err := enc.Encode(card)
		if err != nil {
			log.Fatal().Err(err).Msg("error encoding card")
		}
	}
}

func formatAddress(street, city, state, postal, country string) string {
	formatted := fmt.Sprintf(";;%s;%s;%s;%s;%s", street, city, state, postal, country)
	return formatted
}

func csvReader(path string) (contacts []Contact) {
	csvfile, err := os.Open(path)
	if err != nil {
		log.Fatal().Err(err).Msg("error opening csv file")
	}
	defer csvfile.Close()

	if err := gocsv.UnmarshalFile(csvfile, &contacts); err != nil {
		log.Fatal().Err(err).Msg("error unmarshalling csv file")
	}
	return
}
